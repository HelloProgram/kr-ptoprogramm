package rühmatöö1;
import java.util.ArrayList;
import java.util.Arrays;
public class Märgid{ // klass tekstides/sõnumites sisalduvate märkide jaoks
	protected ArrayList<Character> märgid; 
	Märgid(){
		märgid = new ArrayList<Character>();
		märgid.addAll(Arrays.asList('1','2','3','4','5','6','7','8','9','0','q','w','e','r',
				't','y','u','i','o','p','ü','õ','a','s','d','f','g','h','j','k','l','ö','ä',
				'z','x','c','v','b','n','m','Q','W','E','R','T','Y','U','I','O','P','Ü','Õ',
				'A','S','D','F','G','H','J','K','L','Ö','Ä','Z','X','C','V','B','N','M',',',
				'.','-','+','_',' ','!','"','#','¤','%','&','/','(',')','=','?','@','£','$',
				'€','{','[',']','}','<','>',';',':','|','�','�','˛','ˇ')); //See peaks olema tavaliselt kasutatav konstruktor
	}
	Märgid(ArrayList<Character> märgid){ //Konstruktor kui mingi teist tüüpi märgid
		this.märgid = märgid;
	}
	public void lisamärk(char märk){ //märgi lisamiseks meetod
		märgid.add(märk);
	}
	ArrayList<Character> getMärgid() {
		return märgid;
	}
}
