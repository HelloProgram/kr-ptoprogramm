package rühmatöö1;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class WhereTheMagicHappens { //a.k.a. peaklass
	public static void main(String[] args) {
		Object[] possibleValues = { "Krüpteeri", "Dekrüpteeri"};
		Object selectedValue = JOptionPane.showInputDialog(null,
				"Vali tegevus", "Valik",
				JOptionPane.INFORMATION_MESSAGE, null,
				possibleValues, possibleValues[0]);
		// System.out.println(selectedValue);

		if (selectedValue=="Krüpteeri"){

			String krüpttekst = JOptionPane.showInputDialog(null,"Sisesta krüpteeritav tekst","Andmete sisestamine",JOptionPane.QUESTION_MESSAGE);
			int võtii = Integer.parseInt( JOptionPane.showInputDialog(null,"Sisesta võtme number (peab olema väiksem kui 59)","Andmete sisestamine",JOptionPane.INFORMATION_MESSAGE));

			while ( võtii > 58){

				JOptionPane.showMessageDialog(null, "Võtmeindeks liiga suur, ebasobiv võti", "alert", JOptionPane.ERROR_MESSAGE);
				võtii = Integer.parseInt( JOptionPane.showInputDialog(null,"Sisesta võtme number (peab olema väiksem kui 59)","Andmete sisestamine",JOptionPane.INFORMATION_MESSAGE));

			}

			ArrayList<Võtmed> vőtmelist = new ArrayList<Võtmed>();// List kuhu vőiks lisada vőtmed
			Märgid charid = new Märgid(); //loome oma märkide jaoks listi, kasutades standardkonstruktorit
			Võtmed esimene = new Võtmed("asdf", võtii); // esimese vőtme loomine
			vőtmelist.add(esimene); // esimese vőtme lisamine
			String abc = krüpteeri(krüpttekst,esimene,charid);
			JOptionPane.showMessageDialog(null, abc, "Krüpteeritud tekst on", JOptionPane.INFORMATION_MESSAGE);
			System.out.println(abc);
			JOptionPane.showMessageDialog(null, võtii, "Võtme number on ", JOptionPane.INFORMATION_MESSAGE);
			//   System.out.println(võtii);
		}

		if  (selectedValue=="Dekrüpteeri"){
			String tekst = JOptionPane.showInputDialog(null,"Sisesta dekrüpteeritav tekst","Andmete sisestamine",JOptionPane.QUESTION_MESSAGE);
			Märgid charid = new Märgid(); //loome oma märkide jaoks listi, kasutades standardkonstruktorit
			int võtiii = Integer.parseInt( JOptionPane.showInputDialog(null,"Sisesta võtme number (peab olema väiksem kui 59)","Andmete sisestamine",JOptionPane.INFORMATION_MESSAGE));
			while (võtiii > 58){
				JOptionPane.showMessageDialog(null, "Võtmeindeks liiga suur, ebasobiv võti", "alert", JOptionPane.ERROR_MESSAGE);
				võtiii = Integer.parseInt( JOptionPane.showInputDialog(null,"Sisesta võtme number (peab olema väiksem kui 59)","Andmete sisestamine",JOptionPane.INFORMATION_MESSAGE));
			}

			Võtmed teine = new Võtmed("aaa", võtiii);
			String ffd = dekrüpteeri( tekst,teine,charid);
			JOptionPane.showMessageDialog(null, ffd, "Derüpteeritud tekst on", JOptionPane.INFORMATION_MESSAGE);

		}
	}
	static String krüpteeri(String tekst,Võtmed võti,Märgid charid){ //meetod krüpteerimiseks
		char[] list1 = tekst.toCharArray(); //loome tekstist charide massiivi
		ArrayList<Character> väljundlist = new ArrayList<Character>(); //list, kuhu lugeda sisse krüpteeritud charid
		for(char elem: list1){
			int eleindeks = charid.getMärgid().indexOf(elem); //otsime elemendi indeksi oma märkide listist
			if (eleindeks==-1){} //Kui listis elementi pole, ei toimu midagi
			else if (eleindeks+ võti.getVõtmeindeks()<(charid.getMärgid().size())){ //Kontrollime, kas elemendi indeks + võtme indeks mahub märkide listi sisse
				elem = charid.getMärgid().get(eleindeks+ võti.getVõtmeindeks()); //Kui mahtus, siis muudame elementi märkide listist võtmeindeksi koha võrra
			}
			else if(eleindeks+ võti.getVõtmeindeks()>=(charid.getMärgid().size())){ 
				elem = charid.getMärgid().get(charid.getMärgid().size()-eleindeks-1); //Kui ei mahtunud, siis muudame elemendi märkide listi alguses olevaks elemendiks , vastavalt elemendi indeksile
			}
			else{}
			väljundlist.add(elem);
		}
		String väljund = ""; //Loome stringi väljastamiseks
		for(char a: väljundlist){ //Lisame väljundlisti charid väljundi stringi
			väljund += a;
		}
		return väljund; //tagastame väljundistringi
	}
	static String dekrüpteeri(String tekst,Võtmed võti,Märgid charid){ //meetod dekrüpteerimiseks, toimib sarnaselt krüpteerimise omale, aga vastupidiselt
		char[] list1 = tekst.toCharArray();
		ArrayList<Character> väljundlist = new ArrayList<Character>();
		for(char elem: list1){
			int eleindeks = charid.getMärgid().indexOf(elem);
			if (eleindeks==-1){}
			else if(eleindeks <võti.getVõtmeindeks()){
				elem = charid.getMärgid().get(charid.getMärgid().size()-eleindeks-1);
			}
			else{
				elem = charid.getMärgid().get(eleindeks- võti.getVõtmeindeks());
			}
			väljundlist.add(elem);
		}
		String väljund = "";
		for(char a: väljundlist){
			väljund += a;
		}
		return väljund;
	}
}




